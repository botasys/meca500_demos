"""Prints the readings of a Bota Systems EtherCAT sensor.

Usage: python bota_basic_example.py <adapter>

This example expects a physical slave layout according to
_expected_slave_layout, see below.
"""

import sys
import struct
import time
from multiprocessing import Process, Event, Array
import numpy as np
from collections import namedtuple
import ctypes
import pysoem


class BotaEtherCATSensor:
    BOTA_VENDOR_ID = 0xB07A
    BOTA_PRODUCT_CODE = 0x00000001
    SINC_LENGTH = 64
    # Note that the time step is set according to the sinc filter size!
    time_step = 1.0

    def __init__(self, ifname, sinc_length: int = SINC_LENGTH):
        self._ifname = ifname
        self.sinc_length = sinc_length

        # multiprocessing
        data_shape = (13,)

        # Process for reading hardware
        self.reader_process = None

        self.data_shape = data_shape
        self.running_event = Event()  # Event to control the reading process
        self.data_ready_event = Event()  # Event to signal data readiness
        self.request_zero_event = Event()

        # Using multiprocessing Array (shared array of ctypes doubles)
        self.shared_array = Array(ctypes.c_double, data_shape[0])  # Shared array of doubles

        self.wrench = np.zeros(6)
        self.wrench_sum = np.zeros(6)
        self.wrench_bias = np.zeros(6)
        self.zero_wrench_requested = False
        self.avg_counter = 0

        # Process for reading hardware
        self.reader_process = None

    def bota_sensor_setup(self, slave_pos):
        slave = self._master.slaves[slave_pos]

        ## Set sensor configuration
        # calibration matrix active
        slave.sdo_write(0x8010, 1, bytes(ctypes.c_uint8(1)))
        # temperature compensation
        slave.sdo_write(0x8010, 2, bytes(ctypes.c_uint8(0)))
        # IMU active
        slave.sdo_write(0x8010, 3, bytes(ctypes.c_uint8(1)))

        ## Set force torque filter
        # FIR disable
        slave.sdo_write(0x8006, 2, bytes(ctypes.c_uint8(1)))
        # FAST enable
        slave.sdo_write(0x8006, 3, bytes(ctypes.c_uint8(0)))
        # CHOP enable
        slave.sdo_write(0x8006, 4, bytes(ctypes.c_uint8(0)))
        # Sinc filter size
        slave.sdo_write(0x8006, 1, bytes(ctypes.c_uint16(self.sinc_length)))

        ## Get sampling rate
        sampling_rate = struct.unpack('h', slave.sdo_read(0x8011, 0))[0]
        print("Sampling rate {}".format(sampling_rate))
        if sampling_rate > 0:
            self.time_step = 1.0 / float(sampling_rate)

        print("time step {}".format(self.time_step))

    def _processdata_thread(self):
        self._actual_wkc = 0
        self._master = pysoem.Master()
        self._master.in_op = False
        self._master.do_check_state = False
        SlaveSet = namedtuple('SlaveSet', 'name product_code config_func')
        self._expected_slave_layout = {0: SlaveSet('BFT-MEDS-ECAT-M8', self.BOTA_PRODUCT_CODE, self.bota_sensor_setup)}

        self._master.open(self._ifname)

        if not self._master.config_init() > 0:
            self._master.close()
            raise BasicExampleError('no slave found')

        for i, slave in enumerate(self._master.slaves):
            if not ((slave.man == self.BOTA_VENDOR_ID) and
                    (slave.id == self._expected_slave_layout[i].product_code)):
                self._master.close()
                raise BasicExampleError('unexpected slave layout')
            slave.config_func = self._expected_slave_layout[i].config_func
            slave.is_lost = False

        self._master.config_map()

        if self._master.state_check(pysoem.SAFEOP_STATE, 50000) != pysoem.SAFEOP_STATE:
            self._master.close()
            raise BasicExampleError('not all slaves reached SAFEOP state')

        self._master.state = pysoem.OP_STATE

        zero_requested = False

        while self.running_event.is_set():

            start_time = time.perf_counter()
            self._master.send_processdata()
            self._actual_wkc = self._master.receive_processdata(2000)
            if not self._actual_wkc == self._master.expected_wkc:
                print('incorrect wkc')

            sensor_input_as_bytes = self._master.slaves[0].input
            status = struct.unpack_from('B', sensor_input_as_bytes, 0)[0]

            warningsErrorsFatals = struct.unpack_from('I', sensor_input_as_bytes, 1)[0]

            Fx = struct.unpack_from('f', sensor_input_as_bytes, 5)[0]
            Fy = struct.unpack_from('f', sensor_input_as_bytes, 9)[0]
            Fz = struct.unpack_from('f', sensor_input_as_bytes, 13)[0]
            Mx = struct.unpack_from('f', sensor_input_as_bytes, 17)[0]
            My = struct.unpack_from('f', sensor_input_as_bytes, 21)[0]
            Mz = struct.unpack_from('f', sensor_input_as_bytes, 25)[0]
            forceTorqueSaturated = struct.unpack_from('H', sensor_input_as_bytes, 29)[0]

            Ax = struct.unpack_from('f', sensor_input_as_bytes, 31)[0]
            Ay = struct.unpack_from('f', sensor_input_as_bytes, 35)[0]
            Az = struct.unpack_from('f', sensor_input_as_bytes, 39)[0]
            accelerationSaturated = struct.unpack_from('B', sensor_input_as_bytes, 43)[0]

            Rx = struct.unpack_from('f', sensor_input_as_bytes, 44)[0]
            Ry = struct.unpack_from('f', sensor_input_as_bytes, 48)[0]
            Rz = struct.unpack_from('f', sensor_input_as_bytes, 52)[0]
            angularRateSaturated = struct.unpack_from('B', sensor_input_as_bytes, 56)[0]

            temperature = struct.unpack_from('f', sensor_input_as_bytes, 57)[0]

            self.data = np.array([Fx, Fy, Fz, Mx, My, Mz, Ax, Ay, Az, Rx, Ry, Rz, temperature])
            self.wrench = self.data[:6]

            if self.request_zero_event.is_set():
                if not zero_requested:
                    zero_requested = True
                    self.avg_counter = 0
                    self.wrench_sum = np.zeros(6)
                self.avg_counter += 1
                if self.avg_counter < 50:
                    continue
                self.wrench_sum = self.wrench_sum + self.wrench
                if self.avg_counter == 250:
                    self.wrench_bias = np.divide(self.wrench_sum, 200)
                    print("Wrench zeroing finished with bias: ", self.wrench_bias)
                    self.request_zero_event.clear()
                    zero_requested = False

            self.wrench = self.wrench - self.wrench_bias
            self.data[:6] = self.wrench
            self.shared_array[:] = self.data[:]

            # Signal that new data is ready
            self.data_ready_event.set()

            time_diff = time.perf_counter() - start_time
            if time_diff < self.time_step:
                BotaEtherCATSensor._sleep(self.time_step - time_diff)

    def run(self):
        """Start the hardware reading process"""
        self.running_event.set()
        self.reader_process = Process(target=self._processdata_thread)
        self.reader_process.start()

    def zero_wrench(self):
        print("Started zeroing wrench: DO NOT TOUCH THE SENSOR NOW")
        self.wrench_bias = np.zeros(6)
        self.avg_counter = 0
        self.request_zero_event.set()
        while self.request_zero_event.is_set():
            time.sleep(0.01)
        print("You can now touch the sensor")

    @staticmethod
    def _sleep(duration, get_now=time.perf_counter):
        now = get_now()
        end = now + duration
        while now < end:
            now = get_now()

    def stop(self):
        """Stop the hardware reading process"""
        self.running_event.clear()
        if self.reader_process:
            self.reader_process.join()

    def get_data(self):
        """Get current data from shared memory after ensuring it is ready"""
        if self.data_ready_event.wait(timeout=1):  # Wait for the data to be ready
            self.data_ready_event.clear()
            return np.copy(self.shared_array[:])
        else:
            return None

class BasicExampleError(Exception):
    def __init__(self, message):
        super(BasicExampleError, self).__init__(message)
        self.message = message
