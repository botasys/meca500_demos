import sys
import os
package_path = os.path.dirname(os.path.dirname(__file__))
sys.path.append(package_path)
print(package_path)

from typing import Union, Optional, Type

import tkinter as tk
from tkinter import ttk, font, messagebox
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

from PIL import Image, ImageTk

import pysoem
import serial.tools.list_ports

import numpy as np

from gui.plotter import Plotter

from libraries.bota.bota_ethercat import BotaEtherCATSensor, BasicExampleError
from libraries.bota.bota_serial import BotaSerialSensor, BotaSerialSensorError

from libraries.meca import robot_common, robot as rb

from tasks.task import Task
from tasks.hand_guidance_ethercat import HandGuidance as HandGuidanceEtherCAT
from tasks.hand_guidance_serial import HandGuidance as HandGuidanceSerial
from tasks.move_to_contact_ethercat import MoveToContact as MoveToContactEtherCAT
from tasks.move_to_contact_serial import MoveToContact as MoveToContactSerial

SERIAL = "Serial"
ETHERCAT = "EtherCAT"

class BotaWindow(tk.Tk):
    def __init__(self):
        super().__init__()

        self.wm_title("Bota Systems: Meca500 Demos")
        self.geometry("1280x720")
        self.resizable(False, False)

        self.plotter = Plotter()
        self.task: Optional[Task] = None
        self.sensor: Union[BotaSerialSensor, BotaEtherCATSensor, None] = None

        self.sensor_type = tk.StringVar()
        self.sensor_port_desc = tk.StringVar()
        self.ports = dict()
        self.sensor_config_params = dict()
        self.sensor_config_params["sinc_length"] = 128

        self.meca_robot = rb.Robot()
        self.meca_ip_entry: Optional[tk.Entry] = None

        self.sensor_connected: bool = False
        self.sensor_status_light_canvas: Optional[tk.Canvas] = None
        self.sensor_status_light_oval = None
        self.sensor_status_label_text = tk.StringVar()
        self.meca_connected: bool = False
        self.meca_status_light_canvas: Optional[tk.Canvas] = None
        self.meca_status_light_oval = None
        self.meca_status_label_text = tk.StringVar()
        self.frequency_label_text = tk.StringVar()

        self.style = ttk.Style()
        self.style.configure('.', font=('Helvetica', 12))
        bigfont = font.Font(family="Helvetica", size=12)
        self.option_add("*Font", bigfont)

        bota_logo_path = os.path.join(os.path.dirname(__file__), "images", "Bota-LogoFull-Black.png")
        bota_logo_image = Image.open(bota_logo_path)
        new_width = 125
        bota_logo_image = bota_logo_image.resize((new_width, int(bota_logo_image.size[1] * new_width / bota_logo_image.size[0])))
        self.bota_logo = ImageTk.PhotoImage(bota_logo_image)

        bota_icon_path = os.path.join(os.path.dirname(__file__), "images", "Bota-LogoSquare-Black.png")
        bota_icon_image = Image.open(bota_icon_path)
        new_width = 25
        bota_icon_image = bota_icon_image.resize((new_width, int(bota_icon_image.size[1] * new_width / bota_icon_image.size[0])))
        self.bota_icon = ImageTk.PhotoImage(bota_icon_image)

        self.protocol("WM_DELETE_WINDOW", self.stop_sequence)
        self.iconphoto(False, self.bota_icon)
        self.organize_widgets()

    def stop_sequence(self):
        if self.sensor:
            self.sensor.stop()
        if self.task:
            self.task.stop()
        self.meca_robot.Disconnect()
        self.meca_robot.WaitDisconnected()
        self.plotter.stop()
        self.quit()

    def update_sensor_connected(self, connected: bool):
        self.sensor_connected = connected
        if self.sensor_connected:
            self.sensor_status_light_canvas.itemconfig(self.sensor_status_light_oval, fill="green")
            self.sensor_status_label_text.set("Sensor Connected")
            self.frequency_label_text.set("Sampling Rate: {:0.2f} Hz".format(1.0/(self.sensor_config_params["sinc_length"]*0.00001953125)))
        else:
            self.sensor_status_light_canvas.itemconfig(self.sensor_status_light_oval, fill="red")
            self.sensor_status_label_text.set("Sensor Not Connected")
            self.frequency_label_text.set("Sampling Rate: --")

    def update_meca_connected(self, connected: bool):
        self.meca_connected = connected
        if self.meca_connected:
            self.meca_status_light_canvas.itemconfig(self.meca_status_light_oval, fill="green")
            self.meca_status_label_text.set("Meca500 Connected")
        else:
            self.meca_status_light_canvas.itemconfig(self.meca_status_light_oval, fill="red")
            self.meca_status_label_text.set("Meca500 Not Connected")

    def kill_tasks(self):
        if self.task:
            self.task.stop()
        if self.sensor:
            self.sensor.stop()
            self.sensor = None
        self.meca_robot = rb.Robot()
        self.update_sensor_connected(False)
        self.update_meca_connected(False)
        self.plotter.stop()

    def add_task(self, task: Type[Task]) -> bool:
        if self.sensor:
            self.sensor.stop()
            self.sensor = None
        if self.meca_connected:
            self.meca_robot.Disconnect()
            self.meca_robot.WaitDisconnected()
        self.update_meca_connected(False)
        self.update_sensor_connected(False)

        self.kill_tasks()
        try:
            selected_port = self.ports[self.sensor_port_desc.get()]
        except KeyError:
            messagebox.showerror("Error", "Select valid port!")
            return False

        self.task = task(selected_port, self.meca_ip_entry.get(), self.sensor_config_params)
        self.sensor = self.task.sensor
        self.update_sensor_connected(True)
        self.meca_robot = self.task.robot

        if self.task.initialize():
            self.update_meca_connected(True)
            self.meca_robot.WaitHomed()

            self.plotter.add_sensor(self.sensor)
            return True
        else:
            return False

    def run_task(self):
        self.plotter.run()
        if self.task:
            self.task.run()

    def create_bota_logo_frame(self, root: Union[ttk.Frame, tk.Tk]) -> ttk.Frame:
        frame = ttk.Frame(root)
        logo_label = tk.Label(frame, image=self.bota_logo)
        logo_label.pack(side=tk.TOP, fill=tk.X, expand=True, pady=10)

        return frame

    def create_plotter_frame(self, root: Union[ttk.Frame, tk.Tk]) -> ttk.Frame:
        frame = ttk.Frame(root)

        canvas = FigureCanvasTkAgg(self.plotter.fig, master=frame)
        canvas.draw()
        canvas.get_tk_widget().pack(fill=tk.BOTH, expand=True)

        return frame

    def create_status_frame(self, root: Union[ttk.Frame, tk.Tk]) -> ttk.Frame:
        frame = ttk.Frame(root)

        def create_sensor_connected_frame() -> ttk.Frame:
            subframe = ttk.Frame(frame)

            self.sensor_status_light_canvas = tk.Canvas(subframe, width=20, height=20)
            self.sensor_status_light_oval = self.sensor_status_light_canvas.create_oval(2, 2, 18, 18, fill="red")
            self.sensor_status_light_canvas.pack(side=tk.LEFT, expand=True, padx=5, pady=5)

            label = ttk.Label(subframe, textvariable=self.sensor_status_label_text, font=("Helvetica", 10))
            self.sensor_status_label_text.set("Sensor Not Connected")
            label.pack(side=tk.LEFT, expand=True, padx=5, pady=5)

            return subframe

        sensor_connected_frame = create_sensor_connected_frame()
        sensor_connected_frame.pack(side=tk.LEFT, expand=True, padx=5, pady=5)

        def create_meca_connected_frame() -> ttk.Frame:
            subframe = ttk.Frame(frame)

            self.meca_status_light_canvas = tk.Canvas(subframe, width=20, height=20)
            self.meca_status_light_oval = self.meca_status_light_canvas.create_oval(2, 2, 18, 18, fill="red")
            self.meca_status_light_canvas.pack(side=tk.LEFT, expand=True, padx=5, pady=5)

            label = ttk.Label(subframe, textvariable=self.meca_status_label_text, font=("Helvetica", 10))
            self.meca_status_label_text.set("Meca500 Not Connected")
            label.pack(side=tk.LEFT, expand=True, padx=5, pady=5)

            return subframe

        meca_connected_frame = create_meca_connected_frame()
        meca_connected_frame.pack(side=tk.RIGHT, expand=True, padx=5, pady=5)

        return frame

    def create_sensor_config_frame(self, root: Union[ttk.Frame, tk.Tk]) -> ttk.Frame:
        frame = ttk.Frame(root)

        frame.columnconfigure(0, weight = 1)
        frame.columnconfigure(1, weight = 1)
        frame.columnconfigure(2, weight = 1)

        main_label = ttk.Label(frame, text = "Sensor Configuration", font=("Helvetica", 12, "bold"))
        main_label.grid(column=0, row=0, columnspan=3)

        frequency_label = ttk.Label(frame, textvariable=self.frequency_label_text, font=("Helvetica", 10))
        self.frequency_label_text.set("Sampling Rate: --")
        frequency_label.grid(column=2, row=0)

        sensor_types = [SERIAL, ETHERCAT]
        sensor_type_combobox = ttk.Combobox(frame, textvariable=self.sensor_type, values=sensor_types)
        sensor_type_combobox.set("Select sensor type")
        sensor_type_combobox.grid(column=0, row=1, columnspan=2, sticky=tk.W+tk.E, padx=5, pady=5)

        def config_sensor() -> tk.Toplevel:
            if not self.sensor_type.get() == SERIAL and not self.sensor_type.get() == ETHERCAT:
                messagebox.showwarning("Warning", "Choose a sensor type before!")
                return

            options_window = tk.Toplevel(self)
            options_window.grab_set()

            options_window.columnconfigure(0, weight=4)
            options_window.columnconfigure(1, weight=1)

            options_label = ttk.Label(options_window, text="Sensor Configuration Parameters",
                                      font=("Helvetica", 12, "bold"))
            options_label.grid(column=0, row=0, columnspan=2, padx=5, pady=5)

            def callback(P):
                if str(P).isnumeric():
                    return True
                return False

            vcmd = (options_window.register(callback))

            sinc_length_label = ttk.Label(options_window, text="SINC Length:")
            sinc_length_label.grid(column=0, row=1, padx=5, pady=5, sticky=tk.W)
            sinc_length_entry = ttk.Entry(options_window, validate='all', validatecommand=(vcmd, '%P'))
            sinc_length_entry.insert(0, str(self.sensor_config_params["sinc_length"]))
            sinc_length_entry.grid(column=1, row=1, padx=5, pady=5, sticky=tk.W+tk.E)

            def update_params():
                temp_sampling_rate = 1. / (0.00001953125 * float(str(sinc_length_entry.get())))
                if (self.sensor_type.get() == SERIAL and not (100 <= temp_sampling_rate <= 700)) or (self.sensor_type.get() == ETHERCAT and not (150 <= temp_sampling_rate <= 2000)):
                    messagebox.showwarning("Warning", "For Serial sensors, ensure SINC length is within 74 and 512.\n\nFor EtherCAT, SINC length should be within 26 and 341.")
                    return

                self.sensor_config_params["sinc_length"] = int(sinc_length_entry.get())
                options_window.destroy()

            run_button = ttk.Button(options_window, text="Set parameters", command=update_params)
            run_button.grid(column=0, row=2, columnspan=2, padx=5, pady=5)

            return options_window

        reload_ports_button = ttk.Button(frame, text="Config Sensor", command = config_sensor)
        reload_ports_button.grid(column=2, row=1, sticky=tk.W+tk.E, padx=5, pady=5)


        sensor_port_combobox = ttk.Combobox(frame, textvariable = self.sensor_port_desc, values = [])
        sensor_port_combobox.set("Select sensor port")
        sensor_port_combobox.grid(column=0, row=2, columnspan=2, sticky=tk.W+tk.E, padx=5, pady=5)

        def update_available_ports(*args):
            self.ports.clear()
            if self.sensor_type.get() == SERIAL:
                ports = serial.tools.list_ports.comports()
                for port in ports:
                    self.ports[port.description] = port.device
            elif self.sensor_type.get() == ETHERCAT:
                ports = pysoem.find_adapters()
                for port in ports:
                    self.ports[str(port.desc, encoding="utf-8")] = port.name
            else:
                messagebox.showwarning("Warning", "Select valid sensor type!")
                return

            sensor_port_combobox.set("Select sensor port")
            sensor_port_combobox.config(values=list(self.ports.keys()))

        sensor_type_combobox.bind("<<ComboboxSelected>>", update_available_ports)

        reload_ports_button = ttk.Button(frame, text="Update Ports", command = update_available_ports)
        reload_ports_button.grid(column=2, row=2, sticky=tk.W+tk.E, padx=5, pady=5)

        def start_sensor():
            if self.sensor_connected:
                messagebox.showwarning("Warning", "Sensor already running! Stop sensor first to restart.")
                return
            try:
                selected_port = self.ports[self.sensor_port_desc.get()]
            except KeyError:
                messagebox.showerror("Error", "Select valid port!")
                return

            if self.sensor_type.get() == SERIAL:
                try:
                    self.sensor = BotaSerialSensor(selected_port, self.sensor_config_params["sinc_length"])
                except BotaSerialSensorError:
                    messagebox.showerror("Error", "Something went wrong while activating Bota Serial Sensor!")
                    return
            elif self.sensor_type.get() == ETHERCAT:
                try:
                    self.sensor = BotaEtherCATSensor(selected_port, self.sensor_config_params["sinc_length"])
                except BasicExampleError:
                    messagebox.showerror("Error", "Something went wrong while activating Bota EtherCAT Sensor!")
                    return
            else:
                messagebox.showwarning("Warning", "Select sensor type and port before starting!")
                return

            self.update_sensor_connected(True)
            self.plotter.add_sensor(self.sensor)
            self.sensor.run()
            self.plotter.run()

        start_sensor_button = ttk.Button(frame, text = "Start Sensor", command=start_sensor)
        start_sensor_button.grid(column=0, row=3, sticky=tk.W+tk.E, padx=5, pady=5)


        def zero_sensor():
            if self.sensor:
                self.sensor.zero_wrench()
            else:
                messagebox.showwarning("Warning", "No active sensor available!")

        zero_sensor_button = ttk.Button(frame, text = "Zero Sensor", command=zero_sensor)
        zero_sensor_button.grid(column=1, row=3, sticky=tk.W+tk.E, padx=5, pady=5)

        def stop_sensor():
            if self.sensor:
                self.sensor.stop()
            else:
                messagebox.showwarning("Warning", "No active sensor available!")
            self.update_sensor_connected(False)
            self.sensor = None

        stop_sensor_button = ttk.Button(frame, text = "Stop Sensor", command=stop_sensor)
        stop_sensor_button.grid(column=2, row=3, sticky=tk.W+tk.E, padx=5, pady=5)

        return frame

    def create_meca_config_frame(self, root: Union[ttk.Frame, tk.Tk]) -> ttk.Frame:
        frame = ttk.Frame(root)

        label = ttk.Label(frame, text = "Meca500 Configuration", font=("Helvetica", 12, "bold"))
        label.pack(side=tk.TOP, expand=True, padx=5, pady=5)
        # label.grid(column=0, row=0, columnspan=3)

        def create_meca_ip_frame() -> ttk.Frame:
            subframe = ttk.Frame(frame)

            meca_ip_label = ttk.Label(subframe, text = "Meca500 IP Address:")
            meca_ip_label.pack(side=tk.LEFT, fill=tk.X, padx=5, pady=5)

            self.meca_ip_entry = ttk.Entry(subframe)
            self.meca_ip_entry.insert(0, "192.168.1.100")
            self.meca_ip_entry.pack(side=tk.LEFT, fill=tk.X, expand=True, padx=5, pady=5)

            return subframe

        meca_ip_frame = create_meca_ip_frame()
        meca_ip_frame.pack(side=tk.TOP, fill=tk.X, expand=True, padx=5, pady=5)

        def create_meca_control_panel_frame() -> ttk.Frame:
            subframe = ttk.Frame(frame)

            subframe.columnconfigure(0, weight=1)
            subframe.columnconfigure(1, weight=1)
            subframe.columnconfigure(2, weight=1)

            def activate_meca():
                try:
                    self.meca_robot.Connect(address=self.meca_ip_entry.get())
                    self.meca_robot.SetTRF(0, 0, 0, 0, 0, -22)
                    self.meca_robot.SetVelTimeout(0.10)

                except robot_common.CommunicationError:
                    messagebox.showerror("Error", "Something went wrong while activating the Meca500 robot!")
                    return

                self.update_meca_connected(True)

                self.meca_robot.ActivateAndHome()
                messagebox.showinfo("Info", "Wait for robot to comeplete homing sequence before sending other commands to the robot.")

                self.meca_robot.WaitHomed()
                messagebox.showinfo("Info", "Meca500 sucessfully activated and homed.")

            activate_meca_button = ttk.Button(subframe, text="Activate", command=activate_meca)
            # activate_meca_button.pack(side=tk.LEFT, fill=tk.X, expand=True, padx=5, pady=5)
            activate_meca_button.grid(column=0, row=0, sticky=tk.W+tk.E, padx=5, pady=5)

            def move_to_home():
                if self.meca_connected:
                    self.meca_robot.ClearMotion()
                    self.meca_robot.ResetError()
                    self.meca_robot.ResumeMotion()
                    self.meca_robot.SetJointAcc(30)
                    self.meca_robot.SetCartAcc(50)
                    self.meca_robot.SetJointVel(25)
                    self.meca_robot.MovePose(170, -50, 180, 180, 0, -90)
                    self.meca_robot.SetJointAcc(150)
                    self.meca_robot.SetCartAcc(600)
                    self.meca_robot.SetVelTimeout(0.01)
                else:
                    messagebox.showwarning("Warning", "No connected Meca500 found!")

            move_to_home_button = ttk.Button(subframe, text="Move to Home", command=move_to_home)
            move_to_home_button.grid(column=1, row=0, sticky=tk.W+tk.E, padx=5, pady=5)

            def disconnect_robot():
                if self.meca_connected:
                    self.meca_robot.Disconnect()
                    self.meca_robot.WaitDisconnected()
                    self.update_meca_connected(False)
                else:
                    messagebox.showwarning("Warning", "No connected Meca500 found!")

            disconnect_robot_button = ttk.Button(subframe, text="Disconnect", command=disconnect_robot)
            # disconnect_robot_button.pack(side=tk.LEFT, fill=tk.X, expand=True, padx=5, pady=5)
            disconnect_robot_button.grid(column=2, row=0, sticky=tk.W+tk.E, padx=5, pady=5)

            return subframe

        meca_control_panel_frame = create_meca_control_panel_frame()
        meca_control_panel_frame.pack(side=tk.TOP, fill=tk.X, expand=True)

        return frame

    def create_task_selection_frame(self, root: Union[ttk.Frame, tk.Tk]) -> ttk.Frame:
        frame = ttk.Frame(root)

        label = ttk.Label(frame, text="Example Tasks", font=("Helvetica", 12, "bold"))
        label.pack(side=tk.TOP, expand=True, padx=5, pady=5)

        def create_task_control_panel_frame() -> ttk.Frame:
            subframe = ttk.Frame(frame)

            def start_hand_guidance():
                if self.sensor_type.get() == SERIAL:
                    task = HandGuidanceSerial
                elif self.sensor_type.get() == ETHERCAT:
                    task = HandGuidanceEtherCAT
                else:
                    messagebox.showwarning("Warning", "Choose valid sensor type!")
                    return

                if self.add_task(task):
                    self.run_task()
                    messagebox.showinfo("Info", "You can now move the robot by applying force on the sensor!")

            hand_guidance_button = ttk.Button(subframe, text="Hand Guidance", command=start_hand_guidance)
            hand_guidance_button.pack(side=tk.LEFT, expand = True, padx=5, pady=5)

            def open_move_to_contact_menu() -> tk.Toplevel:
                options_window = tk.Toplevel(self)
                options_window.grab_set()

                options_window.columnconfigure(0, weight=4)
                options_window.columnconfigure(1, weight=1)
                options_window.columnconfigure(2, weight=1)
                options_window.columnconfigure(3, weight=1)
                options_window.columnconfigure(4, weight=1)

                options_label = ttk.Label(options_window, text="Move to Contact Parameters", font=("Helvetica", 12, "bold"))
                options_label.grid(column=0, row=0, columnspan=5, padx=5, pady=5)

                def callback(P):
                    try:
                        _ = float(P)
                        return True
                    except ValueError:
                        return False

                vcmd = (options_window.register(callback))

                force_thresh_label = ttk.Label(options_window, text="Force Threshold")
                force_thresh_label.grid(column=0, row=1, padx=5, pady=5, sticky=tk.W)
                force_thresh_entry = ttk.Entry(options_window, validate='all', validatecommand=(vcmd, '%P'))
                force_thresh_entry.insert(0, "5.0")
                force_thresh_entry.grid(column=1, row=1, padx=5, pady=5, columnspan=3)
                force_thresh_unit_label = ttk.Label(options_window, text="N")
                force_thresh_unit_label.grid(column=4, row=1, padx=5, pady=5, sticky=tk.W)


                search_direction = tk.StringVar()
                search_direction_label = ttk.Label(options_window, text="Search Direction")
                search_direction_label.grid(column=0, row=2, padx=5, pady=5, sticky=tk.W)
                search_direction_X_radio = ttk.Radiobutton(options_window, text="X", value="X", variable=search_direction)
                search_direction_X_radio.grid(column=1, row=2, padx=5, pady=5)
                search_direction_Y_radio = ttk.Radiobutton(options_window, text="Y", value="Y", variable=search_direction)
                search_direction_Y_radio.grid(column=2, row=2, padx=5, pady=5)
                search_direction_Z_radio = ttk.Radiobutton(options_window, text="Z", value="Z", variable=search_direction)
                search_direction_Z_radio.grid(column=3, row=2, padx=5, pady=5)
                search_direction_unit_label = ttk.Label(options_window, text="")
                search_direction_unit_label.grid(column=4, row=2, padx=5, pady=5, sticky=tk.W)
                search_direction.set("Z")
                search_direction_values = {"X": np.array([1, 0, 0]), "Y": np.array([0, 1, 0]), "Z": np.array([0, 0, 1])}

                reference_frame = tk.StringVar()
                reference_frame_label = ttk.Label(options_window, text="Reference Frame")
                reference_frame_label.grid(column=0, row=3, padx=5, pady=5, sticky=tk.W)
                reference_frame_WRF_radio = ttk.Radiobutton(options_window, text="WRF", value="WRF", variable=reference_frame)
                reference_frame_WRF_radio.grid(column=1, row=3, padx=5, pady=5)
                reference_frame_TRF_radio = ttk.Radiobutton(options_window, text="TRF", value="TRF", variable=reference_frame)
                reference_frame_TRF_radio.grid(column=3, row=3, padx=5, pady=5)
                reference_frame_unit_label = ttk.Label(options_window, text="")
                reference_frame_unit_label.grid(column=4, row=3, padx=5, pady=5, sticky=tk.W)
                reference_frame.set("TRF")


                velocity_label = ttk.Label(options_window, text="Velocity")
                velocity_label.grid(column=0, row=4, padx=5, pady=5, sticky=tk.W)
                velocity_entry = ttk.Entry(options_window, validate='all', validatecommand=(vcmd, '%P'))
                velocity_entry.insert(0, "5.0")
                velocity_entry.grid(column=1, row=4, padx=5, pady=5, columnspan=3)
                velocity_unit_label = ttk.Label(options_window, text="mm/s")
                velocity_unit_label.grid(column=4, row=4, padx=5, pady=5, sticky=tk.W)


                max_dist_label = ttk.Label(options_window, text="Maximum Distance")
                max_dist_label.grid(column=0, row=5, padx=5, pady=5, sticky=tk.W)
                max_dist_entry = ttk.Entry(options_window, validate='all', validatecommand=(vcmd, '%P'))
                max_dist_entry.insert(0, "100.0")
                max_dist_entry.grid(column=1, row=5, padx=5, pady=5, columnspan=3)
                max_dist_unit_label = ttk.Label(options_window, text="mm")
                max_dist_unit_label.grid(column=4, row=5, padx=5, pady=5, sticky=tk.W)


                retract_dist_label = ttk.Label(options_window, text="Retract Distance")
                retract_dist_label.grid(column=0, row=6, padx=5, pady=5, sticky=tk.W)
                retract_dist_entry = ttk.Entry(options_window, validate='all', validatecommand=(vcmd, '%P'))
                retract_dist_entry.insert(0, "5.0")
                retract_dist_entry.grid(column=1, row=6, padx=5, pady=5, columnspan=3)
                retract_dist_unit_label = ttk.Label(options_window, text="mm")
                retract_dist_unit_label.grid(column=4, row=6, padx=5, pady=5, sticky=tk.W)

                def update_params():
                    assert (isinstance(self.task, MoveToContactSerial) or isinstance(self.task, MoveToContactEtherCAT)), "self.task is not of type MoveToContact"
                    self.task.configure_params(float(force_thresh_entry.get()),
                                               search_direction_values[search_direction.get()],
                                               reference_frame.get(),
                                               float(velocity_entry.get()),
                                               float(max_dist_entry.get()),
                                               float(retract_dist_entry.get())
                                               )
                    options_window.destroy()

                run_button = ttk.Button(options_window, text="Set parameters", command=update_params)
                run_button.grid(column=0, row=7, columnspan=5, padx=5, pady=5)

                return options_window

            def start_move_to_contact():
                if self.sensor_type.get() == SERIAL:
                    task = MoveToContactSerial
                elif self.sensor_type.get() == ETHERCAT:
                    task = MoveToContactEtherCAT
                else:
                    messagebox.showwarning("Warning", "Choose valid sensor type!")
                    return

                if self.add_task(task):
                    options_window = open_move_to_contact_menu()
                    self.wait_window(options_window)
                    self.run_task()

            move_to_contact_button = ttk.Button(subframe, text="Move to Contact", command=start_move_to_contact)
            move_to_contact_button.pack(side=tk.LEFT, expand = True, padx=5, pady=5)

            return subframe

        task_control_panel_frame = create_task_control_panel_frame()
        task_control_panel_frame.pack(side=tk.TOP, expand=True, padx=5, pady=5)

        def stop_task():
            self.kill_tasks()
            messagebox.showinfo("Info", "Tasks stopped!")

        stop_task_button = ttk.Button(frame, text="Stop Task", command=stop_task)
        stop_task_button.pack(side=tk.TOP, fill=tk.X, expand=True, padx=5, pady=5)

        return frame

    def create_quit_button_frame(self, root: Union[ttk.Frame, tk.Tk]) -> ttk.Frame:
        frame = ttk.Frame(root)

        quit_button = ttk.Button(frame, text = "Quit", command=self.stop_sequence)
        quit_button.pack(side=tk.TOP, fill=tk.X, expand=True, padx=5, pady=5)

        return frame

    def organize_widgets(self):

        plotter_frame = self.create_plotter_frame(self)
        plotter_frame.pack(side=tk.RIGHT, fill=tk.BOTH, expand=False, padx=5, pady=5)

        def create_panel_frame() -> ttk.Frame:
            subframe = ttk.Frame(self)

            bota_logo_frame = self.create_bota_logo_frame(subframe)
            bota_logo_frame.pack(side=tk.TOP, fill=tk.X)

            status_frame = self.create_status_frame(subframe)
            status_frame['borderwidth'] = 5
            status_frame['relief'] = tk.GROOVE
            status_frame.pack(side=tk.TOP, fill=tk.X, expand=True, padx=5, pady=5)

            sensor_config_frame = self.create_sensor_config_frame(subframe)
            sensor_config_frame['borderwidth'] = 5
            sensor_config_frame['relief'] = tk.GROOVE
            sensor_config_frame.pack(side=tk.TOP, fill=tk.X, expand=True, padx=5, pady=5)

            meca_config_frame = self.create_meca_config_frame(subframe)
            meca_config_frame['borderwidth'] = 5
            meca_config_frame['relief'] = tk.GROOVE
            meca_config_frame.pack(side=tk.TOP, fill=tk.X, expand=True, padx=5, pady=5)

            task_selection_frame = self.create_task_selection_frame(subframe)
            task_selection_frame['borderwidth'] = 5
            task_selection_frame['relief'] = tk.GROOVE
            task_selection_frame.pack(side=tk.TOP, fill=tk.X, expand=True, padx=5, pady=5)

            quit_button_frame = self.create_quit_button_frame(subframe)
            quit_button_frame.pack(side=tk.BOTTOM, fill=tk.X, expand=True, padx=5, pady=5)

            return subframe

        panel_frame = create_panel_frame()
        panel_frame.pack(side=tk.RIGHT, expand=True, fill=tk.BOTH)

if __name__ == "__main__":
    bota_window = BotaWindow()
    bota_window.mainloop()
