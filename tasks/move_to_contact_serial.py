import sys
import os
package_path = os.path.dirname(os.path.dirname(__file__))
sys.path.append(package_path)

from typing import Optional, Dict
import numpy as np
from numpy.typing import NDArray
from numpy import linalg as LA
import time
from multiprocessing import Event
from threading import Thread

from tasks.task import Task
from libraries.meca import robot_common, robot as rb
from libraries.bota.bota_serial import BotaSerialSensor as Bota

def limit(n, minn, maxn):
    if n < minn:
        return minn
    elif n > maxn:
        return maxn
    else:
        return n


class MoveToContact(Task):
    MECA_IP = '192.168.1.100'

    def __init__(self, sensor_port, meca_ip = MECA_IP, sensor_config_params: Optional[Dict[str, int]] = None):
        self.stop_event = Event()
        self.master_robot = rb.Robot()
        if sensor_config_params:
            self.bftSensor = Bota(sensor_port, sensor_config_params["sinc_length"])
        else:
            self.bftSensor = Bota(sensor_port)
        self.bftSensor.run()

        self.robot_control_thread = None

        self.meca_ip = meca_ip

        # Move to contact params
        self.force_threshold = 0.
        self.direction = np.zeros(3)
        self.reference_frame = "WRF"
        self.velocity = 0.
        self.max_distance = 0.
        self.retract_distance = 0.

        self.start_pose = np.zeros(6)
        self.surface_pose = np.zeros(6)
        self.reached_surface = False

        self.alpha = 1
        self.wrenchFiltered = np.zeros(6)
        self.wrench = np.zeros(6)

    def activate_robot(self):
        try:
            self.master_robot.Connect(address=self.meca_ip)  # add here also ip. Now is default
            self.master_robot.SetTRF(0, 0, 0, 0, 0, -22)  # todo adjust TRF
            self.master_robot.SetVelTimeout(0.10)

        except robot_common.CommunicationError:
            print("Connection Error Master")
            return False

        print("Master Connected")
        self.master_robot.ActivateAndHome()
        return True

    def move_to_start(self):
        self.master_robot.ClearMotion()
        self.master_robot.ResetError()
        self.master_robot.ResumeMotion()
        self.master_robot.SetJointAcc(30)  # todo check if all are neccesary
        self.master_robot.SetCartAcc(50)
        self.master_robot.SetJointVel(25)
        self.master_robot.SetJointAcc(150)  # todo check if all are neccesary
        self.master_robot.SetCartAcc(600)
        self.master_robot.SetVelTimeout(0.01)

    def configure_params(self, force_threshold: float, direction: NDArray[float], reference_frame: str,velocity: float, max_distance: float, retract_distance: float):
        self.force_threshold = force_threshold
        self.direction = direction / LA.norm(direction)
        assert(reference_frame == "WRF" or reference_frame == "TRF"), "reference_frame should be either 'WRF' or 'TRF'"
        self.reference_frame = reference_frame
        self.velocity = velocity
        self.max_distance = max_distance
        self.retract_distance = retract_distance

    def start_move_to_contact(self):
        self.run()
        try:
            while 1:
                time.sleep(0.1)
        except KeyboardInterrupt:
            print("Stopped")
            self.stop()

    def run(self):
        self.robot_control_thread = Thread(target=self.control_thread)
        self.robot_control_thread.start()

    def stop(self):
        self.bftSensor.stop()
        self.stop_event.set()
        if self.robot_control_thread:
            self.robot_control_thread.join()
        self.master_robot.Disconnect()

    def initialize(self):
        if self.activate_robot():
            self.move_to_start()
            return True
        return False

    def control_thread(self):
        self.bftSensor.zero_wrench()
        self.start_pose = np.array(self.master_robot.GetPose())
        while not self.stop_event.is_set():
            start_time = time.perf_counter()
            data = self.bftSensor.get_data()
            if data is None:
                continue
            self.wrench = data[:6]

            twist = self.move_to_contact_controller()
            if self.reference_frame == "WRF":
                self.master_robot.MoveLinVelWRF(twist[0], twist[1], twist[2], 0., 0., 0.)
            elif self.reference_frame == "TRF":
                self.master_robot.MoveLinVelTRF(twist[0], twist[1], twist[2], 0., 0., 0.)
            time_diff = time.perf_counter() - start_time

    def move_to_contact_controller(self):
        twist = np.zeros(3)

        normF = LA.norm(self.wrench[0:3])

        curr_pose = np.array(self.master_robot.GetPose())

        distance = LA.norm(curr_pose[0:3] - self.start_pose[0:3])
        if distance > self.max_distance:
            return twist

        if not self.reached_surface:
            twist = self.direction * self.velocity
            if normF > self.force_threshold:
                self.reached_surface = True
                self.surface_pose = np.array(self.master_robot.GetPose())

        if self.reached_surface:
            twist = -self.direction * self.velocity
            if LA.norm(self.surface_pose[0:3] - curr_pose[0:3]) > self.retract_distance:
                twist = np.zeros(3)

        return twist

    @property
    def sensor(self):
        return self.bftSensor

    @property
    def robot(self):
        return self.master_robot

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    force_threshold = 5.0       # in N
    search_direction = np.array([0, 0, 1])
    reference_frame = "TRF"     # "TRF" or "WRF"
    velocity = 5.0              # in mm/s
    max_distance = 100.0        # in mm
    retract_distance = 5.0      # in mm

    if len(sys.argv) == 2:
        controller = MoveToContact(sys.argv[1])
        controller.initialize()
        controller.configure_params(force_threshold, search_direction, reference_frame, velocity, max_distance, retract_distance)
        controller.start_move_to_contact()
    elif len(sys.argv) >= 3:
        controller = MoveToContact(sys.argv[1], sys.argv[2])
        controller.initialize()
        controller.configure_params(force_threshold, search_direction, reference_frame, velocity, max_distance, retract_distance)
        controller.start_move_to_contact()
    else:
        print('usage: main <sensor_portname> <meca_ip_address(optional)>')
        sys.exit(1)
