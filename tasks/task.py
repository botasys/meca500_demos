import sys
import os

package_path = os.path.dirname(os.path.dirname(__file__))
sys.path.append(package_path)

from abc import ABC, abstractmethod
from typing import Union, Dict, Optional

from libraries.bota.bota_serial import BotaSerialSensor
from libraries.bota.bota_ethercat import BotaEtherCATSensor

from libraries.meca import robot as rb

class Task(ABC):
    @abstractmethod
    def __init__(self, sensor_port: str, meca_ip: str, sensor_config_params: Optional[Dict[str, int]] = None):
        pass

    @abstractmethod
    def run(self):
        pass

    @abstractmethod
    def stop(self):
        pass

    @abstractmethod
    def initialize(self) -> bool:
        pass

    @property
    @abstractmethod
    def sensor(self) -> Union[BotaSerialSensor, BotaEtherCATSensor, None]:
        pass

    @property
    @abstractmethod
    def robot(self) -> Union[rb.Robot, None]:
        pass
