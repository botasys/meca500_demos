import sys
import os
package_path = os.path.dirname(os.path.dirname(__file__))
sys.path.append(package_path)

from typing import Optional, Dict
import numpy as np
from numpy import linalg as LA
import time
from threading import Lock, Thread, Event

from tasks.task import Task
from libraries.meca import robot_common, robot as rb
from libraries.bota.bota_serial import BotaSerialSensor as Bota

def limit(n, minn, maxn):
    if n < minn:
        return minn
    elif n > maxn:
        return maxn
    else:
        return n


class HandGuidance(Task):
    MECA_IP = '192.168.1.100'

    def __init__(self, sensor_port, meca_ip = MECA_IP, sensor_config_params: Optional[Dict[str, int]] = None):
        self.lock = Lock()
        self.stop_event = Event()
        self.master_robot = rb.Robot()
        if sensor_config_params:
            self.bftSensor = Bota(sensor_port, sensor_config_params["sinc_length"])
        else:
            self.bftSensor = Bota(sensor_port)
        self.bftSensor.run()

        self.robot_control_thread = None

        self.meca_ip = meca_ip

        # Hand guidance params
        self.gain_tr = 10
        self.gain_rot = 50
        self.alpha = 1
        self.f_threshold_high = 0.2
        self.f_threshold_low = 0.05
        self.m_threshold_high = 0.4
        self.m_threshold_low = 0.1
        self.wrench_filtered = np.zeros(6)
        self.wrench = np.zeros(6)

    def activate_robot(self):
        try:
            self.master_robot.Connect(address=self.meca_ip)  # add here also ip. Now is default
            self.master_robot.SetTRF(0, 0, 0, 0, 0, -22)  # todo adjust TRF
            self.master_robot.SetVelTimeout(0.10)

        except robot_common.CommunicationError:
            print("Connection Error Master")
            return False

        print("Master Connected")
        self.master_robot.ActivateAndHome()
        return True

    def move_to_start(self):
        self.master_robot.ClearMotion()
        self.master_robot.ResetError()
        self.master_robot.ResumeMotion()
        self.master_robot.SetJointAcc(30)  # todo check if all are neccesary
        self.master_robot.SetCartAcc(50)
        self.master_robot.SetJointVel(25)
        self.master_robot.MovePose(170, -50, 180, 180, 0, -90)
        self.master_robot.SetJointAcc(150)  # todo check if all are neccesary
        self.master_robot.SetCartAcc(600)
        self.master_robot.SetVelTimeout(0.01)

    def start_hand_guidance(self):
        self.run()
        try:
            while 1:
                time.sleep(0.1)
        except KeyboardInterrupt:
            print("stopped")
            self.stop()

    def run(self):
        self.robot_control_thread = Thread(target=self.control_thread)
        self.robot_control_thread.start()

    def stop(self):
        self.bftSensor.stop()
        self.stop_event.set()
        if self.robot_control_thread:
            self.robot_control_thread.join()
        self.master_robot.Disconnect()

    def initialize(self) -> bool:
        if self.activate_robot():
            self.move_to_start()
            return True
        return False

    def control_thread(self):
        self.bftSensor.zero_wrench()
        while not self.stop_event.is_set():
            start_time = time.perf_counter()
            data = self.bftSensor.get_data()
            if data is None:
                continue
            self.wrench = data[:6]
            twist = self.hand_guidance_controller()
            # print("twist: ", twist)
            self.master_robot.MoveLinVelTRF(twist[0], twist[1], twist[2], twist[3], twist[4], twist[5])
            time_diff = time.perf_counter() - start_time

    def hand_guidance_controller(self):
        twist = np.zeros(6)
        motiongroup = "NONE"
        self.wrench_filtered = self.wrench_filtered + self.alpha * (self.wrench - self.wrench_filtered)

        normF = LA.norm(self.wrench[0:3])
        normM = LA.norm(self.wrench[3:])

        if normF <= self.f_threshold_low and normM < self.m_threshold_low:
            motiongroup = "NONE"

        if normM > self.m_threshold_high:
            motiongroup = "ROTATION"
        else:
            if motiongroup == "ROTATION" and normM > self.m_threshold_low:
                motiongroup = "ROTATION"
            elif normF > self.f_threshold_high:
                motiongroup = "TRANSLATION"
            elif motiongroup == "ROTATION" and normM < self.m_threshold_low:
                motiongroup = "NONE"
            elif motiongroup == "TRANSLATION" and normF < self.f_threshold_low:
                motiongroup = "NONE"

        match motiongroup:
            case ("NONE"):
                twist = np.zeros(6)
            case ("TRANSLATION"):
                tempDeadzone = (self.wrench_filtered[0:3] / normF * self.f_threshold_low)
                twist[0:3] = self.gain_tr * (self.wrench_filtered[0:3] - tempDeadzone)
                twist[3:] = np.zeros(3)
            case ("ROTATION"):
                tempDeadzone = (self.wrench_filtered[3:] / normF * self.m_threshold_low)
                twist[3:] = self.gain_rot * (self.wrench_filtered[3:] - tempDeadzone)
                twist[0:3] = np.zeros(3)

        for i in range(3):
            twist[i] = limit(twist[i], -60, 60)
            twist[i + 3] = limit(twist[i + 3], -45, 45)
        return twist

    @property
    def robot(self):
        return self.master_robot

    @property
    def sensor(self):
        return self.bftSensor

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    if len(sys.argv) == 2:
        controller = HandGuidance(sys.argv[1])
        controller.initialize()
        controller.start_hand_guidance()
    elif len(sys.argv) >= 3:
        controller = HandGuidance(sys.argv[1], sys.argv[2])
        controller.initialize()
        controller.start_hand_guidance()
    else:
        print('usage: main <sensor_portname> <meca_ip_address(optional)>')
        sys.exit(1)
