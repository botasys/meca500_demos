# Meca500 Demos
This is a package containing a few examples to demonstrate how to run a Meca500 TCP/IP configuration with either the Serial or EtherCAT Bota Systems force-torque sensor.

## Installation
To install the dependencies required by the package, use the following command,
```bash
# On Windows
pip install -r requirements.txt

# On Linux
sudo pip install -r requirements.txt
```
**NOTE**: Administrator privileges are needed to install `pysoem` in Linux, because scripts that use PySOEM need to be executed under adminitrator privileges. 

**NOTE**: For Serial sensors, it is recommended to change the latency settings of the sensor port. You can take the following steps to ensure that there is minimal latency in the sensor communication.

*On Windows*
1. Open Device Manager
2. Open Ports -> Your Sensor's Port -> Properties -> Port Settings -> Advanced -> Latency Timer
3. Select latency to be minimum value (1 msec)

*On Linux*
```bash
setserial <your port name> low_latency
```


### Operating system based dependencies
#### Linux

- Python 3
- GCC (installed on your machine)
- NOTE: Python scripts that use PySOEM must be executed under administrator privileges (for GUI and EtherCAT files)

#### Windows

* [Python 3 / 64 Bit](https://www.python.org/downloads/) or through Microsoft Store
* [Npcap](https://nmap.org/npcap/)* or [WinPcap](https://www.winpcap.org/)

[*] Make sure you check "Install Npcap in WinPcap API-compatible Mode" during the installation.



## Examples
### 1. Hand Guidance

#### Description

1. When the demo file is launched, it connects with the Meca robot and the F/T sensor and activates both of them.
2. After the robot is activated, we perform a homing operation if it has not been done since the most recent power cycle, after which the robot goes to a default pose.
3. Once the robot is at the default pose, the sensor readings are zeroed out. **NOTE**: Do not touch the sensor for a few seconds after the robot reaches the default pose.
4. After a few seconds of waiting, you can move the robot around by pulling on the sensor.

#### Parameters
| Parameter Name     | Default Value | Description                                                                   |
|--------------------|:-------------:|-------------------------------------------------------------------------------|
| `f_threshold_high` |     0.20      | Threshold force norm needs to cross for robot to react to it.                 |
| `f_threshold_low`  |     0.05      | If force norm is less than this threshold, it stops reacting to force.        |
| `m_threshold_high` |     0.40      | Threshold moment norm needs to cross for robot to react to it.                |
| `m_threshold_low`  |     0.10      | If moment norm is less than this threshold, it stops reacting to momentforce. |
| `gain_tr`          |     10.0      | Proportional gain for translation                                             |
| `gain_rot`         |     50.0      | Proportional gain for rotation                                                |
| `alpha`            |     1.00      | Low pass filter parameter                                                     |

### 2. Move to Contact

#### Description

1. When the demo file is launched, it connects with the Meca robot and the F/T sensor and activates both of them.
2. After the robot is activated, we perform a homing operation if it has not been done since the most recent power cycle, after which the robot goes to a default pose.
3. Once the robot is at the default pose, the sensor readings are zeroed out. **NOTE**: Do not touch the sensor for a few seconds after the robot reaches the default pose.
4. After a few seconds of waiting, robot moves in the requested direction until it makes contact with a surface.

#### Parameters
| Parameter Name     | Default Value | Description                                                                                            |
|--------------------|:-------------:|--------------------------------------------------------------------------------------------------------|
| `force_threshold`  |     5.00      | Robot registers contact when force norm crosses this threshold (in N).                                 |
| `search_direction` |    [0,0,1]    | Direction in which robot moves until contact.                                                          |
| `reference_frame`  |     "TRF"     | Reference frame for direction vector (can be "WRF" or "TRF", world frame and tool frame respectively). |
| `velocity`         |     5.00      | Velocity with which robot moves in requested direction (in mm/s).                                      |
| `max_distance`     |     100.0     | Maximum distance robot moves (in mm).                                                                  |
| `retract_distance` |     5.00      | Once contact is registered, robot move back by this distance (in mm).                                  |

## Usage
You can run the examples either through the GUI application, or directly through the command line.

### GUI
To run the GUI application, run the following command
```bash
# On Windows
python gui/main.py

# On Linux
sudo su
python gui/main.py
```
This should launch the application window.

![gui-screenshot](gui/images/docs/gui-help.png)

### Command Line
Running the examples through the command line has the following template,
```bash
# For EtherCAT sensors
### On Windows
python examples/<task_name>_ethercat.py <sensor_port_name> <meca_ip_address>
### On Linux
sudo python examples/<task_name>_ethercat.py <sensor_port_name> <meca_ip_address>


# For Serial sensors - same on Windows and Linux
python examples/<task_name>_serial.py <sensor_port_name> <meca_ip_address>
```
The `meca_ip_address` parameter is optional, and can be skipped. Instead, the default value '192.168.1.100' will be used.

The `task_name` correspond to examples as shown in the following table. The table also inform where one can change the parameters discussed above for the examples.

| Example         |    `task_name`     | Parameter Location                                                               |
|-----------------|:------------------:|----------------------------------------------------------------------------------|
| Hand Guidance   |  `hand_guidance`   | Line `38` in both `hand_guidance_ethercat.py` and `hand_guidance_serial.py`      |
| Move to Contact | `move_to_contact`  | Line `162` in both `move_to_contact_ethercat.py` and `move_to_contact_serial.py` |

To list out all available ports and finding the sensor port use the utility functions as follows,
```bash
# For EtherCAT sensors
python utils/find_ethernet_adapters.py

# For Serial sensors
python utils/find_serial_ports.py
```
NOTE: On Linux systems, run EtherCAT scripts with administrator privileges.

Serial ports may look like `COM5` on Windows and like `dev/ttyUSB0` on Linux.
Ethernet ports may look like `\Device\NPF_{XXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}` on Windows, and like `enp0s31f6` on Linux.
